const express = require("express");
const { ObjectID } = require("mongodb");
const cors = require('cors')
const app = express();
const mongoose = require("mongoose");
const _ = require('lodash');  
const api_routes = require('./routes/router')

require('dotenv').config()
require('dotenv/config');

app.use(express.json())
app.listen(process.env.PORT, () => {
          mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true,useUnifiedTopology: true }, (error, client) => {
              if(error) {
                  throw error;
              }   }); });
app.use('/question_answer',api_routes)
app.use('/admin_control',api_routes)           
console.log("server running on " +process.env.PORT )
    

