const express = require('express')
const control = require('../controllers/controllers')
const control_admin = require('../controllers/admin_control')
const _ = require('lodash');  
const router = express.Router()

//control
router.post('/Ask_Question',control.AskQuestion)
router.put('/post_answer/:Item_code/:questionid',control.PostAnswer)
router.get('/get_question/:Item_code/:questionid',control.GetQuestionId)
router.get('/get_question/:Item_code',control.GetQuestion)
router.put('/updating_like/:user_name/:answer_id',control.Like)
router.put('/updating_unlike/:user_name/:answer_id',control.Unlike)
router.delete('/delete',control.Delete)

//admin_control
router.put('/update_answer/:Item_code',control_admin.UpdateAnswer)
router.delete('/delete/:status_check/',control_admin.delete_status)
router.get('/get_final_ques_ans/',control_admin.get_final_ques_ans)



module.exports= router