const express = require('express')

const mongoose= require('mongoose')

const { Schema } = require('mongoose')


const autoIncrement = require("mongoose-auto-increment-cn");



const like_user_array = new Schema({

   like_user : {type: String, default: "null"},



})

const unlike_user_array = new Schema({

   unlike_user : {type: String},


})




const answer_detail = new Schema({
   // Answer_id : {type : Number},

    Answer_name : {type : String,required : true},
    like_count : {type: Object, default:0},
    unlike_count : {type: Object, default:0},


    user_name : {type : String, required : true},
    liked_user : [like_user_array ],
    unliked_user : [unlike_user_array],
     status: {
        enum : [0,1],
        type : Number, 
        default:0
   
   }
})



const Question = new Schema({


   created: {type: Date, default: Date.now()},



   //ProductId : {type: Number, required : true },
   question : {type : String, required : true},
   questioned_user_name : {type : String, required : true},
   Answer  :  [answer_detail],
   status : {
      enum : [0,1],
      type: Number , 
      default: 0 
   
   },
   Item_code: {type: Number, required : true }





});


   




   

module.exports = mongoose.model('customer_question', Question);
