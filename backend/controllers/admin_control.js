const model = require('../models/model')

UpdateAnswer = async(req,res) => {
  if(req.body.ansId){
   const update_status1 = await model.updateMany({ "Answer._id" : req.body.ansId },{$set : {"Answer.$.Answer_name" : req.body.new_answer,"Answer.$.status" : req.body.status_answer }})
    }
 if(req.body.questionid){
   const update_question = await model.updateMany({Item_code : req.params.Item_code,_id : req.body.questionid} ,  {question : req.body.question , status : req.body.status_ques })
    }
    res.send("updated")
   };
 delete_status = async (req,res) => {
     const status =0;
     const disable_question = await model.find({status : status,_id : req.body.question_id})
     console.log(disable_question)
    if(disable_question.length == 1 ){
       await model.deleteMany({_id : req.body.question_id })
     }
     const disable_answer = await model.findOne({Answer : {$elemMatch : { _id : req.body.answer_id,status :status}}})
     console.log(disable_answer)
     if(disable_answer){
        await model.updateMany({$pull : {"Answer": { "_id" : req.body.answer_id}}})
      }
     res.status(201).json({
      message : "successfully updated"
     });

  } 
get_final_ques_ans = async(req,res) => {
    const status = 1;
    const all_ans =
    await model.findOne({Answer : {$elemMatch : { _id : req.body.answer_id,status :status}}})
    .select({
      "Answer": true,
      "Item_code" : true,
      "question": true,
      "question_id" : true,
      "status" : true, 
  }).exec(); 
  res.send(all_ans)
  }
  module.exports = {
    UpdateAnswer,
    delete_status,
    get_final_ques_ans,
}

