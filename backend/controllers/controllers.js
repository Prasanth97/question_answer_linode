
const model = require('../models/model')
const _ = require('lodash');  


AskQuestion = async(req,res) => {
 
    const aldready_asked_question = await model.find({question : req.body.question})
    if(!_.isEmpty(aldready_asked_question)){
    
      return res.status(409).json({status:"exist" })
      }
    try{
    const feedback = await model.insertMany({Item_code : req.body.Item_code,question : req.body.question, questioned_user_name : req.body.questioned_user_name,status : req.body.status }) ;
      res.status(200).send(feedback)
    } catch(e) {
      return res.status(403).json({status: 'failed', msg :e})
    } 
    };
PostAnswer = async(req, res, next) => { 
     var check_question = await model.find({Item_code : req.params.Item_code, _id:req.params.questionid})
     if(check_question.length == 0){
          return res.send("question not found").status(404)
       }

       if(check_question =! null){
       try{
       const  post_answer = await model.updateMany({Item_code: req.params.Item_code,_id:req.params.questionid}, {$push: { Answer : req.body.Answer}})
       res.status(200).send(post_answer)

      } catch(err) {
        return res.status(403).json({status: 'failed', msg :err})
      }

    }
         
        
       };
GetQuestionId = async (req,response) => {
      const get_questions = await model.find( {_id : req.params.questionid,Item_code: req.params.Item_code })
     .select({
             "Item_code" : true,
             "question": true,
             "status"  : true,
             "user_name" : true,
             "Answer" : true
         }).exec(); 
         response.send(get_questions)
      };
GetQuestion = async (req,res) => {
     const get_questions = await model.find({Item_code:req.params.Item_code})
      .select({
             "Answer": true,
             "Item_code" : true,
             "question": true,
             "question_id" : true, 
             "Answer_name": true,
         }).exec(); 
         res.send(get_questions)
      };
Like = async(req,res)=> {
       const update_array_like_user = await model.find({ "Answer.liked_user.like_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
       const update_array_unlike_user = await model.find({ "Answer.unliked_user.unlike_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
       console.log(update_array_like_user.length)
       if(update_array_like_user.length ==0  && update_array_unlike_user.length == 1 ) {
         const update_status = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.like_count" : 1},$push : {"Answer.$.liked_user": { "like_user" : req.params.user_name}}})
         const update_status1 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.unlike_count" : -1},$pull : {"Answer.$.unliked_user": { "unlike_user" : req.params.user_name}}})
       } else if(update_array_like_user.length ==0) {
       const update_status3 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.like_count" : 1},$push : {"Answer.$.liked_user": { "like_user" : req.params.user_name}}})
       }
       res.status(201).json({
       message : "success"
      });
      };
Unlike = async(req,res)=> {

     const update_array_like_user = await model.find({ "Answer.liked_user.like_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
     const update_array_unlike_user = await model.find({ "Answer.unliked_user.unlike_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
     if(update_array_unlike_user.length == 0 && update_array_like_user.length == 1){
       const update_status1 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.unlike_count" : 1},$push : {"Answer.$.unliked_user": { "unlike_user" : req.params.user_name}}})
       const unlike_user_pull1 = await model.updateMany({ "Answer._id" : req.params.answer_id },{$inc : {"Answer.$.like_count" : -1}, $pull : {"Answer.$.liked_user": { "like_user" : req.params.user_name}}})
     } else  if(update_array_unlike_user.length == 0){
       const update_status12 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.unlike_count" : 1},$push : {"Answer.$.unliked_user": { "unlike_user" : req.params.user_name}}})
       res.status(201).json({
     message : "ok"
     });
    }
    };
Delete = async(req,res) => {
    const deleting_question = await model.deleteMany({})
    res.send(deleting_question)
  }
module.exports = {
    AskQuestion,
    PostAnswer,
    GetQuestionId,
    GetQuestion,
    Like,
    Unlike,
    Delete,
}

