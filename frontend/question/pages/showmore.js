import React, { useState, useEffect,  useReducer, useRef } from 'react';
import Axios from 'axios';

export default function Home(props) {
 
  const [showModal, setShowModal] = React.useState(false);
  const [posts, setPosts] = useState([]);

        //Get Call
       // var citrus = posts[0].splice(0,1);   
      useEffect(()=>{
        Axios
        .get("http://localhost:3024/get_question/1")
        .then(res =>{
          // console.log(res)
          setPosts(res.data)

      })
      .catch(err =>{
        console.log(err)
      })
    })
    //end
    useEffect(() => {
      setFilteredCountries(
        posts.filter((post) =>
          post.question.toLowerCase().includes(search.toLowerCase())
        )
      );
    }, [search, posts]);
  
    
  return (

     
    
   
    <div className="col-span-12 lg:col-span-9 bg-white h-auto p-10">
      <div className="xl:flex justify-between">
       <h1 className="font-bold text-xl xl:text-3xl">Questions and Answers</h1>
       <>
       <button
        className="bg-pink-500 flex text-white active:bg-pink-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
        type="button"
        style={{ transition: "all .15s ease" }}
        onClick={() => setShowModal(true)}
      >
        Post Your Question
      </button>
      {showModal ? (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
            onClick={() => setShowModal(true)}>
            <div className="relative  my-6 mx-auto w-1/4">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                  <h4 className="text-2xl font-semibold">
                    Post Your Question
                  </h4>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}>
                    <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">×</span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative  flex-auto">
                <form>
                  <textarea className="border-gray-100 border-solid	w-full text-gray-600 text-lg leading-relaxed overflow-y-hidden h-40" value={data.question} onChange={(e) => handle(e)} id="question" >
                   
                  </textarea>
                  <div className="flex items-center justify-end p-6  border-t border-solid border-gray-300 rounded-b">
                  {/* <button
                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}>
                    Close
                  </button> */}
                  <button onClick={_onvalueSubmit}
                    className="bg-green-500  text-white active:bg-green-600 font-bold uppercase text-sm px-9 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}>
                    Submit
                  </button>
                </div>
                  </form>
                </div>
                {/*footer*/}
               
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
          
        </>
      ) : null}
    </>
    </div>
    </div>

