import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


export default function productcontainer() {
    return (
        <div className="question-left">
      <img className="m-auto" src="https://rukminim1.flixcart.com/image/246/246/kekadu80/mobile/x/n/y/oppo-f17-pro-cph2119-original-imafv7pxhjkcrv3q.jpeg?q=70"></img>
      <div className="pl-4 pt-4 text-xl	">
      <h5>POCO M2 (Pitch Black, 64 GB)  (6 GB RAM)</h5>
      <div className="flex">
      <button className=" bg-green-500 flex text-white active:bg-pink-600 font-bold uppercase text-base	 px-1 py-0.5 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 "
            style={{ transition: "all .15s ease" }}
         >4.2&nbsp;<strong style={{fontSize:'10px'}}><FontAwesomeIcon icon="star" className="text-xs	" /></strong> </button><span style={{color:'gray', fontSize:'15px', fontWeight:'500'}}>(15,345)</span>
         </div>
      <p>Rs. 669<span></span></p>
      </div>
      </div>
    )
}
<style jsx>
  {`
  .question-left h5
  {
    font-size:15px!important;
  }

  ` }
</style>