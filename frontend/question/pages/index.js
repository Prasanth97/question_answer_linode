import React, { useState, useEffect,  useReducer, useRef } from 'react';
import Axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Productleft from './product-container';
import { useForm } from "react-hook-form";
import {useRouter} from 'next/router'

export default function Home(props) {
  const router = useRouter()
  const [modal, setModal] = React.useState(false);
  const [showmoremodal, setShowmoremodal] = React.useState(false);
  const [ansshowModal, anssetShowModal] = React.useState(false);
  const [posts, setPosts] = useState([]);
  const [currQus, setCurrQus] = useState({});
  const [showcurrQus, setShowcurrQus] = useState({});
  const [filteredCountries, setFilteredCountries] = useState([]);
  const [search, setSearch] = useState("");  
  const [count, setCount] = useState(0);

  const handleClose = ()=>{
    console.log('handleClose',handleClose)
    setModal(false);
  } 
  const handleAddClick = ()=>{
    console.log('handleClose',handleClose)
    setModal(true);
  } 
  const showhandleClose = ()=>{
    setShowmoremodal(false);
  } 

  const showhandleAddClick = (post) => {
    setShowmoremodal(true);
    console.log(post)
    setShowcurrQus(post);
  };

const handleModel = (flag, post) => {
  setCurrQus(post);
  anssetShowModal(flag);
};

  //Answer Start
  const [Ansdata, AnssetData] = useState(
    {
      Answer_name: "",   
        }
  );
  const Anshandle = (e) => {
    const Ansnewdata = { ...Ansdata };
    Ansnewdata[e.target.id] = e.target.value;
    console.log(e.target.id);
    console.log(e.target.value);
    AnssetData(Ansnewdata)
  }
  const _AnsonvalueSubmit=async(id)=>{
    console.log(Ansdata,"ggg", id);
    window.location.reload(false);

  let Ansparams=  {
      "ProductId" : 1,
      "question" : "",
      
     "Answer": [
                {
                    "Answer_id": 1,
                    "Answer_name": Ansdata.Answer_name,
                }
                ]
      }
     console.log('posts', posts);
     var qObj = posts.find(p => p._id === id);
     console.log('qObj', qObj);
      var response = await Axios.put("http://localhost:3024/post_answer/13/" + qObj._id, Ansparams)
      .then(res => res) 
      .catch(e => console.log(e,"error"));
      console.log(response, "ressss")
  }
    //End Answer
    
  

    //Question Start

      const [data, setData] = useState(
      
        {
     question: "",
    ProductId: "",
    
            }
      );
      const handle = (e) => {
        
        const newdata = { ...data };
        newdata[e.target.id] = e.target.value;
        console.log(e.target.id);
        console.log(e.target.value);
        setData(newdata)
    
      }
      const { register, handleSubmit, errors } = useForm();
        const onSubmit=(e)=>{
        window.location.reload(false);
        // e.preventdefault();
        router.push('/');
        console.log(data,"ggg");
        let params={
          "question":data.question,
          "ProductId": 1,
        };
        Axios.post("http://localhost:3024/Ask_Question/" , params)
          .then(res => {
            // console.log(res.data);
          })
          // history.push('/profile')
 
      }

    //End

        //Get Call
       // var citrus = posts[0].splice(0,1);   
      useEffect(()=>{
        Axios
        .get("http://localhost:3024/get_question/13/123456")
        .then(res =>{
          // console.log(res)
          setPosts(res.data)

      })
      .catch(err =>{
        console.log(err)
      })
    })
    //end
    useEffect(() => {
      setFilteredCountries(
        posts.filter((post) =>
          post.question.toLowerCase().includes(search.toLowerCase())
        )
      );
    }, [search, posts]);
  
    
  return (

    <div className="grid grid-cols-12 gap-4 p-10 bg-gray-100">
     
     <div className="col-span-12 lg:col-span-3 pt-10 bg-white h-auto">
       <Productleft />
</div>
   
    <div className="col-span-12 lg:col-span-9 bg-white h-auto p-10">
      <div className="xl:flex justify-between">
       <h1 className="text-xl xl:text-2xl">Questions and Answers</h1>

       <>
       
       {showmoremodal ? (
         <>
           <div   
             className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
             >
           <div>
  <div className="py-4 mt-6 bg-gray-50 shadow h-64" style={{width:'700px', overflowY:'scroll'}}>
<p className="mb-1 pt-6 pb-6 pl-6" style={{borderBottom:'1px solid whitesmoke'}}><span class="pr-2">Q : </span>{showcurrQus.question}</p>
<div class="collapse showmore" id="collapseExample" aria-ex                                                                                                                                                         panded="false">
<p className="bg-#000-50  leading-7 head noselect" >
{showcurrQus.Answer.map((p, index) =>(
      <p className="py-2 pl-6" style={{borderBottom:'1px solid whitesmoke', color:'#000000b0'}}><span class="font-semibold" key={index}>A : </span>
       {p.Answer_name} 
       <div className="flex justify-between ">
    <div>
<p className="font-semibold mb-2" style={{color:'#9CA3AF'}}>Anonymous</p>
<p className="flex font-bold" style={{color:'#4B5563'}}><span><FontAwesomeIcon icon="user" /></span><span className="ml-2">Poorvika Seller</span></p>
            </div>
            <div className="flex mr-6">
      <button onClick={() => console.log(p._id)} >
        <strong><FontAwesomeIcon icon="thumbs-up" /></strong>
        {count}
  
      </button>
      <button className="outline-none" onClick={() => setCount(count - 1)} >
          <strong><FontAwesomeIcon icon="thumbs-down" /></strong>
          {count}
        &nbsp;|&nbsp;
      </button>
    </div>
            </div>
      </p>
    ))}
    </p>
   
    </div>
   
</div>
</div>
 
           </div>
           <div className="opacity-25 fixed inset-0 z-40 bg-black" onClick={showhandleClose}></div>
         </>
       ) : null}
     </>




       <>
       
      {modal ? (
        <>
          <div   
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
            >
            <div className="relative  my-6 mx-auto w-4/12">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                  <h4 className="text-xl">
                    Post Your Question
                  </h4>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}>
                    <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">×</span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative  flex-auto">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <textarea name="lastname"  ref={register({required: "Required",minLength: 10})
  }  className="border-gray-100 border-solid p-6 pb-0	 w-full text-gray-600 text-lg leading-relaxed overflow-y-hidden h-40" value={data.question} onChange={(e) => handle(e)} id="question" >
                   
                  </textarea>

                  <div className="border-solid border-gray-300  border-t">
                  <span style={{color:'#ef4445'}} className="pl-4">{errors.lastname && 'Please provide more details on your question'}</span>

  <div className="flex items-center justify-end p-6 py-4 rounded-b">
                      <button onClick={handleClose}
                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}>
                    Close
                  </button>
                  <input type="submit" 
                    className="bg-green-500  text-white active:bg-green-600 font-bold uppercase text-sm px-9 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                    style={{ transition: "all .15s ease" }} />
                  
                </div>
                </div>
                  </form>
                </div>
                {/*footer*/}
               
              </div>
            </div>

          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
       </div>
       <div className="mt-8">
  <form method="GET">
    <div className="relative text-gray-600 focus-within:text-gray-400">
      <span className="absolute inset-y-0 left-0 flex items-center pl-2">
        <button type="submit" className="p-1 focus:outline-none focus:shadow-outline">
          <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-5 h-5"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
        </button>
      </span>
      <input type="text"
        placeholder="Search Countries"
        onChange={(e) => setSearch(e.target.value)}
      type="search" name="q" className="py-3 text-sm rounded-md pl-10 shadow rounded border-solid border-1 border-light-blue-500 qa-search" placeholder="Search..." autocomplete="off" style={{border: '1px solid #d2d2d2', width: '100%'}}/>
    </div>
  </form>
</div>




  {filteredCountries.map(post =>(
<div>
  <div className="py-4 mt-6 bg-gray-50 shadow h-auto">
<p className="mb-1 pt-6 pb-6 pl-6" style={{borderBottom:'1px solid whitesmoke'}}><span class="pr-2">Q : </span>{post.question}</p>
<div class="collapse showmore" id="collapseExample" aria-expanded="false">
<p className="bg-#000-50  leading-7 head noselect" >{post.Answer.slice(0,1).map((p, index) =>(
      <p className="py-2 pl-6" style={{borderBottom:'1px solid whitesmoke', color:'#000000b0'}}><span class="font-semibold" key={index}>A : </span>
       {p.Answer_name} 
       <div className="flex justify-between ">
    <div>
<p className="font-semibold mb-2" style={{color:'#9CA3AF'}}>Anonymous</p>
<p className="flex font-bold" style={{color:'#4B5563'}}><span><FontAwesomeIcon icon="user" /></span><span className="ml-2">Poorvika Seller</span></p>
            </div>
            <div className="flex mr-6">
      <button onClick={() => console.log(p._id)} >
        <strong><FontAwesomeIcon icon="thumbs-up" /></strong>
        {count}
  
      </button>
      <button className="outline-none" onClick={() => setCount(count - 1)} >
          <strong><FontAwesomeIcon icon="thumbs-down" /></strong>
          {count}
        &nbsp;|&nbsp;
      </button>
    </div>
            </div>
      </p>
    ))}</p>
   
    </div>
    <div className="flex justify-between p-4">
       <>
      <button
        className="bg-pink-500 flex text-white active:bg-pink-600 font-bold uppercase text-sm px-10 py-2 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 "
        type="button"
        style={{ transition: "all .15s ease" }}
        onClick={() => handleModel(true, post)}>Reply</button> 
        <button className="bg-pink-500 flex text-white active:bg-pink-600 font-bold uppercase text-sm px-10 py-2 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 "
        type="button" onClick={() => showhandleAddClick(post)}>showmore</button>
    </>
       </div>
</div>
</div>
))}
<button
        className="bg-pink-500 mt-10 flex text-white active:bg-pink-600 uppercase text-sm px-4 py-2 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
        type="button"
        style={{ transition: "all .15s ease" }}
        onClick={handleAddClick}
      >
        Post Your Question
      </button>
{ansshowModal ? (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
            >
            <div className="relative my-6 mx-auto w-1/4">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t ">
                  <h3 className="text-2xl font-semibold">
                    Post Your Answer
                  </h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => anssetShowModal(false)}>
                  </button>
                </div>
                {/*body*/}
                <form  >
        <textarea className="border-gray-100 border-solid	w-full text-gray-600 text-lg leading-relaxed overflow-y-hidden h-40 p-6" style={{border:"none"}} value={Ansdata.Answer_name} onChange={(e) => Anshandle(e)} id="Answer_name" >
        </textarea>
        <div className="flex items-center justify-end p-6 border-t border-solid border-gray-300 rounded-b">
                  {/* <button
                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }} onClick={() => anssetShowModal(true)}>
                    Close
                  </button> */}
  <button onClick={() => anssetShowModal(false)}
                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}>
                    Close
                  </button>
                  <button  onClick={() =>_AnsonvalueSubmit(currQus._id)} 
                    className="bg-green-500 text-white active:bg-green-600 font-bold uppercase text-sm px-9 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                    type="button"
                    style={{ transition: "all .15s ease" }}>
                    Submit
                  </button>
                </div>
      </form>
                {/*footer*/}
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}

    </div>
    <style jsx>{`
      .accor .body {
        background: #fafafa;
        padding: 0 20px;
        max-height: 0;
        overflow: hidden;
        transition: 200ms ease-in-out;
      }
      .accor.active > .body {
        padding: 10px 20px;
        max-height: 600px;
      }
      
      .noselect {
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
        user-select: none; /* Non-prefixed version, currently
                                        supported by Chrome and Opera */
      }
      textarea:focus, input:focus{
        outline: none;
    }
    textarea
    {
      color: #000;
    font-weight: 100;
    font-size: 15px;
    }
    .close
    {
      position: absolute;
    height: 24px;
    width: 24px;
    right: -40px;
    top: 0;
    padding: 0;
    background: transparent;
    border: none;
    cursor: pointer;
    color: #fff;
    font-size: 32px;
    line-height: 1;
    vertical-align: top;
    }
      `}</style>
  </div>
  )
}


