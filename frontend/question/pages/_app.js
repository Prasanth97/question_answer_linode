import '../styles/index.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faThumbsUp, faThumbsDown, faUser, faStar } from '@fortawesome/free-solid-svg-icons'

function MyApp({ Component, pageProps }) {
  library.add(faThumbsUp, faThumbsDown, faUser, faStar)
  return <Component {...pageProps} />
}
export default MyApp;



